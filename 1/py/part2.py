#!/usr/bin/env python3

# take the input and split into a list of integers
with open('../input_file') as file:
    input_data = [int(line.rstrip('\n')) for line in file]

frequency = 0
# use a dict to keep track of results
frequency_results = {} 
dupe_found = False

while not dupe_found:
    for entry in input_data:
        frequency += entry
        try:
            if frequency_results[frequency]:
                dupe_found = True
                break
        except:
            frequency_results[frequency] = 'foo'
  
print(frequency)

