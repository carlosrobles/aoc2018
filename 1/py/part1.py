#!/usr/bin/env python3

# take the input and split into a list of integers
with open('../input_file') as file:
    input_data = [int(line.rstrip('\n')) for line in file]

frequency = 0

for entry in input_data:
    frequency += entry

print(frequency)

