package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
)

var lines []string
var frequency int

// error helper function
func check(e error) {
    if e != nil {
        panic(e)
    }
}

// file reader helper function
func readLines(path string) ([]string, error) {
    file, err := os.Open(path)
    check(err)
    defer file.Close()
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines, scanner.Err()
}

func main() {
    lines, err := readLines("../input_file")
    check(err)
    for index, line := range lines {
        line, err := strconv.Atoi(line)
        _ = index
        check(err)
        frequency += line
    }
    fmt.Println(frequency)
}
