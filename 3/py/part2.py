#!/usr/bin/env python3

import re

with open('../input_file') as file:
    input_data = [line.rstrip('\n') for line in file]

# create zero'd out 1000 x 1000 array
# that will track each square inch
fabric_list = [[0 for i in range(0,1000)] for j in range(0,1000)]

patch_list = []
used_twice_or_more = 0

# this list has a list of lists of strings
extracted_list = [re.split(r'(@|,|:|x)', entry) for entry in input_data]

# remove spaces around the strings
for entry in extracted_list:
    patch_list.append([item.strip() for item in entry])

# go through each patch
for entry in patch_list:
    patch_number = (entry[0])
    start_row = int(entry[4])
    start_column = int(entry[2])
    end_row = int(entry[4]) + int(entry[8]) - 1
    end_column = int(entry[2]) + int(entry[6]) - 1
    for i in range(start_row, end_row + 1):
        for j in range(start_column, end_column + 1):
            if fabric_list[i][j] == 0:
                fabric_list[i][j] = 1
            else:
                fabric_list[i][j] = 2

# go through each patch
for entry in patch_list:
    overlap = False
    patch_number = (entry[0])
    start_row = int(entry[4])
    start_column = int(entry[2])
    end_row = int(entry[4]) + int(entry[8]) - 1
    end_column = int(entry[2]) + int(entry[6]) - 1
    for i in range(start_row, end_row + 1):
        for j in range(start_column, end_column + 1):
            if fabric_list[i][j] > 1:
                overlap = True
    if not overlap:
        print(patch_number)
