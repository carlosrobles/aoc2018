#!/usr/bin/env python3

with open('../input_file') as file:
    input_data = [line.rstrip('\n') for line in file]

for index1, loop1_entry in enumerate(input_data):
    for index2, loop2_entry in enumerate(input_data):
        if index1 == index2:
            continue
        match = [letter1 for letter1, letter2 in zip(loop1_entry, loop2_entry) if letter1 == letter2]
        if len(match) == 25:
            winning_match = match

print(''.join(winning_match))
