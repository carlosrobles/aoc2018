#!/usr/bin/env python3

# take the input and split into a list of strings
with open('../input_file') as file:
    input_data = [line.rstrip('\n') for line in file]

checksum_dict = dict(two_matches=0, three_matches=0)

def reset_entry_results_dict():
    return dict(two_matches=False, three_matches=False)

def reset_alphabet_dict():
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    alphabet_dict = {}
    for letter in alphabet:
        alphabet_dict[letter] = 0
    return alphabet_dict

for entry in input_data:
    entry_results_dict = reset_entry_results_dict()
    alphabet_dict = reset_alphabet_dict()

    for letter in entry:
        alphabet_dict[letter] += 1
    for key, value in alphabet_dict.items():
        if value > 2:
            entry_results_dict['three_matches'] = True
        if value == 2:
            entry_results_dict['two_matches'] = True
    if entry_results_dict['three_matches']:
        checksum_dict['three_matches'] += 1
    if entry_results_dict['two_matches']:
        checksum_dict['two_matches'] += 1

print(checksum_dict['two_matches'] * checksum_dict['three_matches'])
